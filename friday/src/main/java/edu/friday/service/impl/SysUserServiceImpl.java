package edu.friday.service.impl;

import edu.friday.common.constant.UserConstants;
import edu.friday.common.exception.CustomException;
import edu.friday.common.result.TableDataInfo;
import edu.friday.dao.SysUserDAO;
import edu.friday.model.SysUser;
import edu.friday.model.vo.SysUserVO;
import edu.friday.service.SysUserService;
import edu.friday.utils.BeanUtils;
import edu.friday.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

//import edu.friday.utils.security.SecurityUtils;

/**
 * 用户 业务层处理
 */
@Service
public class SysUserServiceImpl implements SysUserService {
    private static final Logger log = LoggerFactory.getLogger(SysUserServiceImpl.class);
    @Autowired
    SysUserDAO sysUserDAO;

    /**
     * 根据条件分页查询用户列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    @Override
    public TableDataInfo selectUserList(SysUserVO user, Pageable page) {
        SysUser sysuser = new SysUser();
        BeanUtils.copyPropertiesIgnoreEmpty(user, sysuser);
        sysuser.setDelFlag("0");
        ExampleMatcher exampleMatcher = ExampleMatcher.matching()
                .withMatcher("userName" , ExampleMatcher.GenericPropertyMatchers.contains())
                .withMatcher("phonenumber" , ExampleMatcher.GenericPropertyMatchers.contains());
        Example<SysUser> example = Example.of(sysuser, exampleMatcher);
        Page<SysUser> rs = sysUserDAO.findAll(example, page);

        return TableDataInfo.success(rs.toList(), rs.getTotalElements());
    }

    /**
     * 通过用户名查询用户
     *
     * @param userName 用户名
     * @return 用户对象信息
     */
    @Override
    public SysUser selectUserByUserName(String userName) {
        SysUser sysuser = new SysUser();
        sysuser.setUserName(userName);
        sysuser.setDelFlag("0");
        Example<SysUser> example = Example.of(sysuser);
        return findOne(example);
    }


    /**
     * 通过用户ID查询用户
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    @Override
    public SysUser selectUserById(Long userId) {
        SysUser sysuser = new SysUser();
        sysuser.setUserId(userId);
        sysuser.setDelFlag("0");
        return sysUserDAO.findOne(Example.of(sysuser)).get();
    }

    /**
     * 统计用户的通用方法
     *
     * @param sysuser
     * @return 结果
     */
    @Override
    public String count(SysUser sysuser) {
        Example<SysUser> example = Example.of(sysuser);
        long count = sysUserDAO.count(example);
        if (count > 0) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验用户名称是否唯一
     *
     * @param userName 用户名称
     * @return 结果
     */
    @Override
    public String checkUserNameUnique(String userName) {
        SysUser sysuser = new SysUser();
        sysuser.setUserName(userName);
        return count(sysuser);
    }

    /**
     * 校验用户手机是否唯一
     *
     * @param userInfo 用户信息
     * @return
     */
    @Override
    public String checkPhoneUnique(SysUserVO userInfo) {
        SysUser user = new SysUser();
        BeanUtils.copyProperties(userInfo, user);
        return checkUnique(user);
    }

    /**
     * 校验email是否唯一
     *
     * @param userInfo 用户信息
     * @return
     */
    @Override
    public String checkEmailUnique(SysUserVO userInfo) {
        SysUser user = new SysUser();
        BeanUtils.copyProperties(userInfo, user);
        return checkUnique(user);
    }

    /**
     * 根据字段校验用户是否唯一
     *
     * @param user 用户信息
     * @return
     */
    @Override
    public String checkUnique(SysUser user) {
        Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
        Example<SysUser> example = Example.of(user);
        SysUser info = findOne(example);
        if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue()) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    @Override
    public SysUser findOne(Example<SysUser> example) {
        List<SysUser> list = sysUserDAO.findAll(example, PageRequest.of(0, 1)).toList();
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    /**
     * 校验用户是否允许操作
     *
     * @param user 用户信息
     */
    @Override
    public void checkUserAllowed(SysUserVO user) {
        if (StringUtils.isNotNull(user.getUserId()) && user.isAdmin()) {
            throw new CustomException("不允许操作超级管理员用户");
        }
    }

    /**
     * 新增保存用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    @Transactional
    public boolean insertUser(SysUserVO user) {
        SysUser sysUser = new SysUser();
        BeanUtils.copyProperties(user, sysUser);
        sysUser.setDelFlag("0");
        // 新增用户信息
        sysUserDAO.save(sysUser);
//        BeanUtils.copyProperties(,user);
        user.setUserId(sysUser.getUserId());
        // 新增用户与角色管理
        insertUserRole(user);
        return null == sysUser.getUserId();
    }

    /**
     * 修改保存用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    @Transactional
    public boolean updateUser(SysUserVO user) {
        Long userId = user.getUserId();
        Optional<SysUser> op = sysUserDAO.findById(userId);
        if (!op.isPresent()) {
            return false;
        }
        // 删除用户与角色关联
        sysUserDAO.deleteUserRoleByUserId(userId);
        SysUser sysUser = op.get();
        BeanUtils.copyPropertiesIgnoreNull(user, sysUser);
        // 用户信息
        sysUserDAO.save(sysUser);
        // 新增用户与角色管理
        insertUserRole(user);
        return null == sysUser.getUserId();
    }

    /**
     * 修改用户状态
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    @Transactional
    public boolean updateUserStatus(SysUserVO user) {
        Optional<SysUser> op = sysUserDAO.findById(user.getUserId());
        if (!op.isPresent()) {
            return false;
        }
        SysUser sysUser = op.get();
        sysUser.setStatus(user.getStatus());
        // 用户信息
        sysUserDAO.save(sysUser);
        return null != sysUser.getUserId();
    }

    /**
     * 修改用户基本信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    @Transactional
    public boolean updateUserProfile(SysUser user) {
        Optional<SysUser> op = sysUserDAO.findById(user.getUserId());
        if (!op.isPresent()) {
            return false;
        }
        SysUser sysUser = op.get();
        sysUser.setNickName(user.getNickName());
        sysUser.setPhonenumber(user.getPhonenumber());
        sysUser.setEmail(user.getEmail());
        sysUser.setSex(user.getSex());
        // 用户信息
        sysUserDAO.save(sysUser);
        return null == sysUser.getUserId();
    }

    /**
     * 修改用户头像
     *
     * @param avatar 头像地址
     * @return 结果
     */
    @Transactional
    public boolean updateUserAvatar(String userName, String avatar) {
        SysUser sysUser = new SysUser();
        sysUser.setUserName(userName);
        Example<SysUser> example = Example.of(sysUser);
        sysUser = findOne(example);
        if (null == sysUser) {
            return false;
        }
        sysUser.setAvatar(avatar);
        // 用户信息
        sysUserDAO.save(sysUser);
        return null == sysUser.getUserId();
    }

    /**
     * 重置用户密码
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    @Transactional
    public boolean resetPwd(SysUserVO user) {
        Optional<SysUser> op = sysUserDAO.findById(user.getUserId());
        if (!op.isPresent()) {
            return false;
        }
        SysUser sysUser = op.get();
        sysUser.setPassword(user.getPassword());
        // 用户信息
        sysUserDAO.save(sysUser);
        return null == sysUser.getUserId();
    }

    @Override
    public int deleteUserByIds(Long[] userIds) {
        return 0;
    }

    /**
     * 新增用户角色信息
     *
     * @param user 用户对象
     */
    @Transactional
    public int insertUserRole(SysUserVO user) {
        Long[] roles = user.getRoleIds();
        if (StringUtils.isNull(roles) || roles.length == 0) {
            return 0;
        }
        Long[] userIds = new Long[roles.length];
        Arrays.fill(userIds, user.getUserId());
        return sysUserDAO.batchInsertUserRole(userIds, roles);
    }

    /**
     * 重置用户密码
     *
     * @param userName 用户名
     * @param password 密码
     * @return 结果
     */
    @Override
    public int resetUserPwd(String userName, String password) {
        SysUser user = new SysUser();
        user.setUserName(userName);
        user = findOne(Example.of(user));
        if (user == null) {
            return 0;
        }
        user.setPassword(password);
        sysUserDAO.save(user);
        return 1;
    }

//
//    /**
//     * 导入用户数据
//     *
//     * @param userList 用户数据列表
//     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
//     * @param operName 操作用户
//     * @return 结果
//     */
//    @Override
//    public String importUser(List<SysUser> userList, Boolean isUpdateSupport, String operName)
//    {
//        if (StringUtils.isNull(userList) || userList.size() == 0)
//        {
//            throw new CustomException("导入用户数据不能为空！");
//        }
//        int successNum = 0;
//        int failureNum = 0;
//        StringBuilder successMsg = new StringBuilder();
//        StringBuilder failureMsg = new StringBuilder();
//        String password = "123456";
//        for (SysUser user : userList)
//        {
//            try
//            {
//                // 验证是否存在这个用户
//                SysUser u = userMapper.selectUserByUserName(user.getUserName());
//                if (StringUtils.isNull(u))
//                {
////                    user.setPassword(SecurityUtils.encryptPassword(password));
//                    user.setPassword(password);
//                    user.setCreateBy(operName);
//                    this.insertUser(user);
//                    successNum++;
//                    successMsg.append("<br/>" + successNum + "、账号 " + user.getUserName() + " 导入成功");
//                }
//                else if (isUpdateSupport)
//                {
//                    user.setUpdateBy(operName);
//                    this.updateUser(user);
//                    successNum++;
//                    successMsg.append("<br/>" + successNum + "、账号 " + user.getUserName() + " 更新成功");
//                }
//                else
//                {
//                    failureNum++;
//                    failureMsg.append("<br/>" + failureNum + "、账号 " + user.getUserName() + " 已存在");
//                }
//            }
//            catch (Exception e)
//            {
//                failureNum++;
//                String msg = "<br/>" + failureNum + "、账号 " + user.getUserName() + " 导入失败：";
//                failureMsg.append(msg + e.getMessage());
//                log.error(msg, e);
//            }
//        }
//        if (failureNum > 0)
//        {
//            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
//            throw new CustomException(failureMsg.toString());
//        }
//        else
//        {
//            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
//        }
//        return successMsg.toString();
//    }

}
