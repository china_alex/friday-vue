package edu.friday.dao.custom;

import edu.friday.model.GenTable;
import edu.friday.model.dto.GenTableDTO;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 表 数据层
 */
@Repository
public interface GenTableCustomDAO {

    List<GenTableDTO> selectDbTableList(GenTable genTable, Pageable page);

    long countDbTableList(GenTable genTable);

    List<GenTableDTO> selectDbTableListByNames(String[] tableNames);

}
