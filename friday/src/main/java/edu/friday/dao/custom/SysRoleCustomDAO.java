package edu.friday.dao.custom;

import org.springframework.stereotype.Repository;

/**
 * 角色表 数据层
 */
@Repository
public interface SysRoleCustomDAO {

    int batchInsertRoleMenu(Long[] roles, Long[] menus);
}
