package edu.friday.dao.custom.impl;

import edu.friday.dao.custom.SysUserCustomDAO;
import edu.friday.utils.SqlUtil;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

public class SysUserCustomDAOImpl implements SysUserCustomDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public int batchInsertUserRole(Long[] userIds, Long[] roles) {
        int length = userIds.length > roles.length ? roles.length : userIds.length;
        StringBuffer sql = new StringBuffer();
        sql.append(" insert into sys_user_role(user_id, role_id) values ");
        sql.append(SqlUtil.getBatchInsertSqlStr(length,2));
        Query query = entityManager.createNativeQuery(sql.toString());
        int paramIndex = 1;
        for (int i = 0; i < length; i++) {
            query.setParameter(paramIndex++, userIds[i]);
            query.setParameter(paramIndex++, roles[i]);
        }
        return query.executeUpdate();
    }
}

