package edu.friday.dao.custom;

import org.springframework.stereotype.Repository;

/**
 * 用户表 数据层
 */
@Repository
public interface SysUserCustomDAO  {

    int batchInsertUserRole(Long[] userIds, Long[] roles);
}
