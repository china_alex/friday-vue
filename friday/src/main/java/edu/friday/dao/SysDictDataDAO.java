package edu.friday.dao;

import edu.friday.model.SysDictData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * 字典数据表 数据层
 */
@Repository
public interface SysDictDataDAO extends JpaRepository<SysDictData, Long> {

}
