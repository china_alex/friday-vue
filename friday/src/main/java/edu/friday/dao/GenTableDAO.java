package edu.friday.dao;

import edu.friday.dao.custom.GenTableCustomDAO;
import edu.friday.model.GenTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * 表 数据层
 */
@Repository
public interface GenTableDAO extends JpaRepository<GenTable, Long>, GenTableCustomDAO {

    @Modifying
    @Query(value = " delete from gen_table where table_id in (:tableIds) ", nativeQuery = true)
    void deleteByIds(@Param("tableIds") Long[] tableIds);

}
