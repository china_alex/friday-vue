package edu.friday.dao;

import edu.friday.dao.custom.SysUserCustomDAO;
import edu.friday.model.SysUser;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户表 数据层
 */
@Repository
public interface SysUserDAO extends JpaRepository<SysUser, Long>, SysUserCustomDAO {

    /**
     * 根据条件分页查询用户列表
     *
     * @return 用户信息集合信息
     */
    List<SysUser> findByDelFlag(String delFlag, Pageable page);

    @Modifying
    @Query(value = " update sys_user set del_flag = '2' where user_id in :userIds ", nativeQuery = true)
    int deleteUserByIds(@Param("userIds") Long[] userIds);

    @Modifying
    @Query(value = " delete from sys_user_role where user_id=:userId ", nativeQuery = true)
    int deleteUserRoleByUserId(@Param("userId")Long userId);

//    @Query(value = " ", nativeQuery = true)
//    int insertUserRole(@Param("userId")Long userId,@Param("roleId") Long roleId);
}
