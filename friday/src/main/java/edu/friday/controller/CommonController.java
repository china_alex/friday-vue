package edu.friday.controller;

import edu.friday.common.result.RestResult;
import edu.friday.utils.IdUtils;
import edu.friday.utils.RedisCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @description:
 **/
@RestController
public class CommonController {

    @Autowired
    private RedisCache redisCache;

    @ResponseBody
    @GetMapping("t")
    public RestResult test() {
        return RestResult.success();
    }

    @GetMapping("api")
    public void apiDocs(HttpServletResponse response) throws IOException {
        response.sendRedirect("swagger-ui.html");
    }

    @GetMapping("druid")
    public void druid(HttpServletResponse response) throws IOException {
        response.sendRedirect("/druid/index.html");
    }

    /**
     * 生成验证码
     */
    @GetMapping("/captchaImage")
    public RestResult getCode(HttpServletResponse response) throws IOException {
        // 生成随机字串
//        String verifyCode = "123";
//        verifyCode = VerifyCodeUtils.generateVerifyCode(4);
        // 唯一标识
        String uuid = IdUtils.simpleUUID();
//        String verifyKey = Constants.CAPTCHA_CODE_KEY + uuid;
//        redisCache.setCacheObject(verifyKey, verifyCode, Constants.CAPTCHA_EXPIRATION, TimeUnit.MINUTES);
        RestResult ajax = RestResult.success();
        ajax.put("uuid" , uuid);
        return ajax;
    }

}
